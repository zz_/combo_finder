# combo_finder

Brute-force generates gag combinations and what cogs they are capable of
defeating. Includes organic/nonorganic gags and lured/unlured cogs.

```bash
$ git clone https://gitlab.com/zz_/combo_finder.git
$ cd combo_finder
$ rustup update nightly
$ cargo build --release
$ cargo run --release > combos.txt
$ nvim combos.txt
```

---

![](https://www.gnu.org/graphics/agplv3-155x51.png "GNU AGPL v3.0+ logo")
Licensed under the terms of
[the GNU Affero General Public License version 3+](https://www.gnu.org/licenses/agpl.html).
