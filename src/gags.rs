use std::fmt;


#[derive(Clone, Debug, PartialEq, Eq, Hash)]
pub enum GagType {
    Squirt,
}

#[derive(Clone, PartialEq, Eq, Hash)]
pub struct Gag {
    pub gag_type: GagType,
    pub name:     &'static str,
    pub dmg:      u16,
    pub org:      bool,
}


pub const SQUIRT_GAGS: [Gag; 7] = [
    Gag { gag_type: GagType::Squirt, name: "Squirting Flower", dmg: 4,   org: false },
    Gag { gag_type: GagType::Squirt, name: "Glass of Water",   dmg: 8,   org: false },
    Gag { gag_type: GagType::Squirt, name: "Squirt Gun",       dmg: 12,  org: false },
    Gag { gag_type: GagType::Squirt, name: "Seltzer Bottle",   dmg: 21,  org: false },
    Gag { gag_type: GagType::Squirt, name: "Fire Hose",        dmg: 30,  org: false },
    Gag { gag_type: GagType::Squirt, name: "Storm Cloud",      dmg: 80,  org: false },
    Gag { gag_type: GagType::Squirt, name: "Geyser",           dmg: 105, org: false },
];


impl Gag {
    pub fn new(gag_type: GagType,
               name:     &'static str,
               dmg:      u16,
               org:      bool) -> Self {
        Gag {
            gag_type,
            name,
            dmg,
            org,
        }
    }

    pub fn get_organic(self) -> Self {
        if self.org {
            self
        } else {
            Gag {
                gag_type: self.gag_type,
                name:     self.name,
                dmg:      Gag::org_dmg(self.dmg),
                org:      true,
            }
        }
    }

    pub fn org_dmg(non_org_dmg: u16) -> u16 {
        non_org_dmg + non_org_dmg / 10
    }
}

impl fmt::Debug for Gag {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{}{}",
            if self.org {
                "Organic "
            } else {
                ""
            },
            self.name
        )
    }
}

impl fmt::Display for Gag {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{}{}",
            if self.org {
                "Organic "
            } else {
                ""
            },
            self.name
        )
    }
}
