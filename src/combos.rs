use std::fmt;

use gags::{Gag, GagType, SQUIRT_GAGS};
use maths;


#[derive(Clone, PartialEq, Eq, Hash)]
pub struct Cog {
    pub lvl:   u8,
    pub hp:    u16,
    pub lured: bool,
}

#[derive(PartialEq, Eq, Hash)]
pub struct Combo {
    pub cog:  Cog,
    pub gags: Vec<Gag>,
}


pub const COG_ARRAY: [Cog; 12] = [
    Cog { lvl: 12, hp: 200, lured: false },
    Cog { lvl: 11, hp: 156, lured: false },
    Cog { lvl: 10, hp: 132, lured: false },
    Cog { lvl: 9,  hp: 110, lured: false },
    Cog { lvl: 8,  hp: 90,  lured: false },
    Cog { lvl: 7,  hp: 72,  lured: false },
    Cog { lvl: 6,  hp: 56,  lured: false },
    Cog { lvl: 5,  hp: 42,  lured: false },
    Cog { lvl: 4,  hp: 30,  lured: false },
    Cog { lvl: 3,  hp: 20,  lured: false },
    Cog { lvl: 2,  hp: 12,  lured: false },
    Cog { lvl: 1,  hp: 6,   lured: false },
];


impl Combo {
    pub fn new(cog: Cog, gags: Vec<Gag>) -> Self {
        Combo {
            cog,
            gags,
        }
    }

    pub fn gen_combos(num_toons: usize,
                      num_org:   usize,
                      gag_type:  GagType) -> Vec<Self> {
        let gags = match gag_type {
            GagType::Squirt => SQUIRT_GAGS,
        }.to_vec();

        let mut ret = Vec::with_capacity(
            maths::binom_coef(7 + num_toons, num_toons)
        );

        for gag_vec in maths::multiset_choose(&gags, num_toons) {
            for org_pattern in maths::choose(num_toons, num_org) {
                let mut unlured_dmg = 0;
                let mut lured_dmg   = 0;

                let mut updated_gag_vec = Vec::with_capacity(num_toons);

                for (gag, is_org) in gag_vec.iter().zip(org_pattern.into_iter()) {
                    let updated_gag =
                        if is_org {
                            gag.clone().get_organic()
                        } else {
                            gag.clone()
                        };

                    unlured_dmg += updated_gag.dmg;
                    lured_dmg   += updated_gag.dmg +
                                       (updated_gag.dmg - 1) / 2 + 1;

                    updated_gag_vec.place_back() <- updated_gag;
                }

                if num_toons > 1 { // More than one toon means more than one
                                   // gag of the same type
                    let yellow_dmg = (unlured_dmg - 1) / 5 + 1;

                    unlured_dmg += yellow_dmg;
                    lured_dmg   += yellow_dmg;
                }

                if let Some(lured_cog) = COG_ARRAY.iter().find(|c| c.hp <= lured_dmg) {
                    ret.place_back() <- Combo::new(
                        Cog { lured: true, ..*lured_cog },
                        updated_gag_vec.clone()
                    );

                    if let Some(unlured_cog) = COG_ARRAY.iter().find(|c| c.hp <= unlured_dmg) {
                        ret.place_back() <- Combo::new(
                            unlured_cog.clone(),
                            updated_gag_vec
                        );
                    }
                }
            }
        }

        ret
    }
}

impl fmt::Display for Combo {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{:?} kills a{} level {}",
            self.gags,
            if self.cog.lured {
                " lured"
            } else {
                "n unlured"
            },
            self.cog.lvl
        )
    }
}

impl Cog {
    pub fn hp_by_lvl(lvl: u8) -> u8 {
        if lvl < 12 {
            (lvl + 1) * (lvl + 2)
        } else {
            200
        }
    }
}
