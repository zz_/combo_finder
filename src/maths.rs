pub fn conseq_prod(lower: usize, upper: usize) -> usize {
    let mut ret = 1;

    for n in lower..=upper {
        ret *= n;
    }

    ret
}

pub fn fac(n: usize) -> usize {
    if n < 2 {
        1
    } else {
        conseq_prod(2, n)
    }
}

pub fn binom_coef(n: usize, k: usize) -> usize {
    if k > n {
        return 0;
    }

    let k = k.min(n - k);

    if k < 1 {
        return 1;
    }

    conseq_prod(n - (k - 1), n) / fac(k)
}

pub fn choose(n: usize, k: usize) -> Vec<Vec<bool>> {
    if k < 1 {
        return vec![vec![false; n]];
    }

    if n < 1 {
        return Vec::new();
    }

    let result_count = binom_coef(n, k);
    let mut ret = Vec::with_capacity(result_count);

    for mut rest in choose(n - 1, k) {
        let mut chosen = Vec::with_capacity(n);
        chosen.place_back() <- false;
        chosen.append(&mut rest);

        ret.place_back() <- chosen;
    }

    for mut rest in choose(n - 1, k - 1) {
        let mut chosen = Vec::with_capacity(n);
        chosen.place_back() <- true;
        chosen.append(&mut rest);

        ret.place_back() <- chosen;
    }

    ret
}

pub fn multiset_choose<T: Clone>(set: &Vec<T>, k: usize) -> Vec<Vec<T>> {
    let n = set.len();

    if k < 1 {
        return vec![Vec::new()];
    }

    if n < 1 {
        return Vec::new();
    }

    let result_count = binom_coef(n + k - 1, k);
    let mut ret = Vec::with_capacity(result_count);

    for init in 0..=k {
        for mut rest in multiset_choose(&set[1..].to_vec(), k - init) {
            let mut ms = Vec::with_capacity(k);

            for _ in 0..init {
                ms.place_back() <- set[0].clone();
            }

            ms.append(&mut rest);

            ret.place_back() <- ms;
        }
    }

    ret
}
