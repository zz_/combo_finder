#![feature(collection_placement)]
#![feature(inclusive_range_syntax)]
#![feature(placement_in_syntax)]

mod combos;
mod gags;
mod maths;

use combos::Combo;
use gags::GagType;


fn main() {
    for num_toons in 1..=4 {
        for num_org in 0..=num_toons {
            for result in Combo::gen_combos(num_toons, num_org, GagType::Squirt) {
                println!("{}", result);
            }
        }
    }
}
